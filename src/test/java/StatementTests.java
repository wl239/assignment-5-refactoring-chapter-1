import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;

public class StatementTests {
    @Test
    public void testStatement() {
        String expected = """
                Statement for BigCo
                  Hamlet: $650.00 (55 seats)
                  As You Like It: $580.00 (35 seats)
                  Othello: $500.00 (40 seats)
                Amount owed is $1730.00
                You earned 47 credits
                """;

        Play p1 = new Play("hamlet", "Hamlet", "tragedy");
        Play p2 = new Play("as-like", "As You Like It", "comedy");
        Play p3 = new Play("othello", "Othello", "tragedy");
        ArrayList<Play> pList = new ArrayList<>();
        pList.add(p1);
        pList.add(p2);
        pList.add(p3);
        Performance per1 = new Performance("hamlet", 55);
        Performance per2 = new Performance("as-like", 35);
        Performance per3 = new Performance("othello", 40);
        ArrayList<Performance> perList = new ArrayList<>();
        perList.add(per1);
        perList.add(per2);
        perList.add(per3);
        String customer = "BigCo";
        BillPrint app = new BillPrint(pList, customer, perList);

        assertEquals(expected,app.statement());
    }
}
