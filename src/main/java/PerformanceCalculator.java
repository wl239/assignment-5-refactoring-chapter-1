
public class PerformanceCalculator {
    protected Performance aPerformance;
    protected Play play;
    protected int amount;
    protected int volumeCredits;

    public PerformanceCalculator(Performance aPerformance, Play play) {
        this.aPerformance = aPerformance;
        this.play = play;
        this.volumeCredits = Math.max(aPerformance.getAudience() - 30, 0);
    }

    public Play getPlay() {
        return play;
    }

    public int getAmount() {
        return amount;
    }

    public int getVolumeCredits() {
        return volumeCredits;
    }

    public int amount() {
        throw new Error("Subclass responsibility");
    }
}

class TragedyCalculator extends PerformanceCalculator {
    public TragedyCalculator(Performance aPerformance, Play play) {
        super(aPerformance, play);
        super.amount = amount();
    }

    public int amount() {
        int result = 40000;
        if (super.aPerformance.getAudience() > 30) {
            result += 1000 * (super.aPerformance.getAudience() - 30);
        }
        return result;
    }
}

class ComedyCalculator extends PerformanceCalculator {
    public ComedyCalculator(Performance aPerformance, Play play) {
        super(aPerformance, play);
        super.amount = amount();
        super.volumeCredits = volumeCredits();
    }

    public int amount() {
        int result = 30000;
        if (super.aPerformance.getAudience() > 20) {
            result += 10000 + 500 * (super.aPerformance.getAudience() - 20);
        }
        result += 300 * super.aPerformance.getAudience();
        return result;
    }

    public int volumeCredits() {
        return (int) (super.volumeCredits + Math.floor((double) this.aPerformance.getAudience() / 5.0));
    }
}
