import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * BillPrint - adapted from Refactoring, 2nd Edition by Martin Fowler
 *
 * @author Minglun Zhang
 *
 */
public class BillPrint {
	private final HashMap<String, Play> plays;
	private final String customer;
	private final ArrayList<Performance> performances;

	public BillPrint(ArrayList<Play> plays, String customer, ArrayList<Performance> performances) {
		this.plays = new HashMap<>();
		for (Play p: plays) this.plays.put(p.getId(), p);
		this.customer = customer;
		this.performances = performances;
	}

	public HashMap<String, Play> getPlays() {
		return plays;
	}

	public String getCustomer() {
		return customer;
	}

	public ArrayList<Performance> getPerformances() {
		return performances;
	}

	public Play playFor(Performance aPerformance) {
		Play play = plays.get(aPerformance.getPlayID());
		if (play == null) {
			throw new IllegalArgumentException("No play found");
		}
		return play;
	}

	public String usd(int aNumber) {
		DecimalFormat numberFormat = new DecimalFormat("#.00");
		return numberFormat.format((double) aNumber / 100);
	}

	public int totalVolumeCredits(StatementData data){
		int result = 0;
		for (Performance perf: data.performances) {
			result += perf.getVolumeCredit();
		}
		return result;
	}

	public int totalAmount(StatementData data) {
		int result = 0;
		for (Performance perf: data.performances) {
			result += perf.getAmount();
		}
		return result;
	}

	public PerformanceCalculator createPerformanceCalculator(Performance aPerformance, Play aPlay) {
		return switch (aPlay.getType()) {
			case "tragedy" -> new TragedyCalculator(aPerformance, aPlay);
			case "comedy" -> new ComedyCalculator(aPerformance, aPlay);
			default -> throw new IllegalArgumentException("unknown type: " + aPlay.getType());
		};
	}

	public void enrichPerformance(Performance aPerformance) {
		PerformanceCalculator calculator = createPerformanceCalculator(aPerformance, playFor(aPerformance));
		aPerformance.setPlay(calculator.getPlay());
		aPerformance.setAmount(calculator.getAmount());
		aPerformance.setVolumeCredit(calculator.getVolumeCredits());
	}

	public StatementData createStatementData() {
		StatementData statementData = new StatementData(this.customer, this.performances);
		for (Performance perf: statementData.performances) {
			enrichPerformance(perf);
		}
		statementData.setTotalAmount(totalAmount(statementData));
		statementData.setTotalVolumeCredits(totalVolumeCredits(statementData));
		return statementData;
	}

	public String statement() {
		return renderPlainText(createStatementData());
	}

	public String renderPlainText(StatementData data) {
		StringBuilder result = new StringBuilder("Statement for " + data.customer + "\n");
		for(Performance perf: data.performances) {
			result.append("  ").append(perf.getPlay().getName()).append(": $").append(usd(perf.getAmount())).append(" (").append(perf.getAudience()).append(" seats)").append("\n");
		}
		result.append("Amount owed is $").append(usd(data.getTotalAmount())).append("\n");
		result.append("You earned ").append(data.getTotalVolumeCredits()).append(" credits").append("\n");
		return result.toString();
	}


	public String htmlStatement() {
		return renderHtml(createStatementData());
	}

	public String renderHtml(StatementData data) {
		StringBuilder result = new StringBuilder(String.format("`<h1>Statement for %s</h1>\n", data.customer));
		result.append("<table>\n");
		result.append("<tr><th>play</th><th>seats</th><th>cost</th></tr>");
		for (Performance perf: data.performances) {
			result.append(String.format("<tr><td>%s</td><td>$%s</td>", perf.getPlay().getName(), perf.getAudience()));
			result.append(String.format("<td>%s</td></tr>\n", usd(perf.getAmount())));
		}
		result.append("</table>\n");
		result.append(String.format("<p>Amount owed is <em>%s</em></p>\n", usd(data.getTotalAmount())));
		result.append(String.format("<p>You earned <em>%s</em> credits</p>\n", data.getTotalVolumeCredits()));
		return result.toString();
	}

	public static void main(String[] args) {
		Play p1 = new Play("hamlet", "Hamlet", "tragedy");
		Play p2 = new Play("as-like", "As You Like It", "comedy");
		Play p3 = new Play("othello", "Othello", "tragedy");
		ArrayList<Play> pList = new ArrayList<>();
		pList.add(p1);
		pList.add(p2);
		pList.add(p3);
		Performance per1 = new Performance("hamlet", 55);
		Performance per2 = new Performance("as-like", 35);
		Performance per3 = new Performance("othello", 40);
		ArrayList<Performance> perList = new ArrayList<>();
		perList.add(per1);
		perList.add(per2);
		perList.add(per3);
		String customer = "BigCo";
		BillPrint app = new BillPrint(pList, customer, perList);
		System.out.println(app.statement());
	}

}
