import java.util.ArrayList;

public class StatementData {
    public String customer;
    public ArrayList<Performance> performances;
    private int totalAmount;
    private int totalVolumeCredits;

    public StatementData(String customer, ArrayList<Performance> performances) {
        this.customer = customer;
        this.performances = performances;
    }

    public int getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(int totalAmount) {
        this.totalAmount = totalAmount;
    }

    public int getTotalVolumeCredits() {
        return totalVolumeCredits;
    }

    public void setTotalVolumeCredits(int totalVolumeCredits) {
        this.totalVolumeCredits = totalVolumeCredits;
    }
}
